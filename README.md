# Карта знаний Laravel

Здесь перечислены необходимые знания для различных уровней владения Laravel.

За основу взят [Laravel Learning Path](https://github.com/LaravelDaily/Laravel-Roadmap-Learning-Path) («Путь изучения Laravel»).

---

## Начальный уровень
Создание простого проекта

__Иконки ссылок: :book: Документация :clapper: Видео :page_facing_up: Статья :capital_abcd: Курс :package: Рекомендуемый пакет__

| Тема | Ссылки |
| ----- | ----- |
| **Маршрутизация и контроллеры: Основы** ||
| Функции обратного вызова и Route::view() |:book: [Основы маршрутизации](https://docs.rularavel.com/docs/8.x/routing#basic-routing) <br>:book: [Маршруты представлений](https://docs.rularavel.com/docs/8.x/routing#view-routes) <br>|
| Маршрутизация к контроллеру одиночного действия |:book: [Основные понятия о контроллерах](https://docs.rularavel.com/docs/8.x/controllers#basic-controllers) <br>|
| Параметры маршрута |:book: [Параметры маршрута](https://docs.rularavel.com/docs/8.x/routing#route-parameters) <br>|
| Именованные маршруты |:book: [Именованные маршруты](https://docs.rularavel.com/docs/8.x/routing#named-routes) <br>|
| Группы маршрутов |:book: [Группы маршрутов](https://docs.rularavel.com/docs/8.x/routing#route-groups) <br>|
| **Основы работы с Blade** ||
| Отображение данных |:book: [Blade: Отображение данных](https://docs.rularavel.com/docs/8.x/blade#displaying-data) <br>|
| Операторы If-Else и циклы |:book: [Blade: Операторы If](https://docs.rularavel.com/docs/8.x/blade#if-statements) <br>:book: [Blade: Циклы](https://docs.rularavel.com/docs/8.x/blade#loops) <br>|
| Шаблон: @include, @extends, @section, @yield |:book: [Blade: Макеты с использованием наследования шаблонов](https://docs.rularavel.com/docs/8.x/blade#layouts-using-template-inheritance) <br>|
| Компоненты Blade |:book: [Компоненты Blade](https://docs.rularavel.com/docs/8.x/blade#components) <br>:clapper: [Базовые компоненты](https://www.youtube.com/watch?v=wI-edZod4DQ) <br>|
| **Основы авторизации** ||
| Стартовые наборы: Breeze (Tailwind) или Laravel UI (Bootstrap) |:book: [Laravel Breeze](https://docs.rularavel.com/docs/8.x/starter-kits#laravel-breeze) <br>:book: [Laravel UI](https://github.com/laravel/ui) <br>|
| Фасад Auth |:book: [Получение аутентифицированного пользователя](https://docs.rularavel.com/docs/8.x/authentication#retrieving-the-authenticated-user) <br>|
| Проверка авторизации в контроллере / Blade |:book: [Определение статуса аутентификации пользователя](https://docs.rularavel.com/docs/8.x/authentication#determining-if-the-current-user-is-authenticated) <br>:book: [Blade: Директивы аутентификации](https://docs.rularavel.com/docs/8.x/blade#authentication-directives) <br>|
| Посредник Auth |:book: [Защита маршрутов](https://docs.rularavel.com/docs/8.x/authentication#protecting-routes) <br>|
| **Основы работы с базой данных** ||
| Миграции |:book: [Миграции](https://docs.rularavel.com/docs/8.x/migrations) <br>|
| Основы модели Eloquent и MVC: Controller -> Model -> View |:book: [Eloquent: Начало работы](https://docs.rularavel.com/docs/8.x/eloquent) <br>|
| Отношения: belongsTo / hasMany / belongsToMany |:book: [Отношения Eloquent: Один ко многим](https://docs.rularavel.com/docs/8.x/eloquent-relationships#one-to-many) <br>:book: [Определение обратной связи Один ко многим](https://docs.rularavel.com/docs/8.x/eloquent-relationships#one-to-many-inverse) <br>:book: [Отношения Eloquent: Многие ко многим](https://docs.rularavel.com/docs/8.x/eloquent-relationships#many-to-many) <br>|
| Нетерпеливая загрузка и проблема запросов «N+1» |:book: [Отношения: Нетерпеливая загрузка](https://docs.rularavel.com/docs/8.x/eloquent-relationships#eager-loading) <br>:package: [Laravel N+1 Query Detector](https://github.com/beyondcode/laravel-query-detector) <br>|
| **Простое CRUD-приложение** ||
| Ресурсные контроллеры |:book: [Ресурсные контроллеры](https://docs.rularavel.com/docs/8.x/controllers#resource-controllers) <br>:page_facing_up: [Пример CRUD на Laravel 8](https://www.internet-technologies.ru/articles/primer-crud-na-laravel-8.html) <br>|
| Формы, валидация и запросы форм |:book: [Валидация Laravel](https://docs.rularavel.com/docs/8.x/validation) <br>|
| Загрузка файлов и основы работы с папками хранилища |:book: [Файловая система: Загрузка файлов](https://docs.rularavel.com/docs/8.x/filesystem#file-uploads) <br>|
| Пагинация |:book: [База данных: Постраничная навигация](https://docs.rularavel.com/docs/8.x/pagination) <br>|
| **В качестве бонуса** ||
| Лучшие практики |:page_facing_up: [Соглашения об именах в Laravel](https://laravel.demiart.ru/laravel-naming-conventions/) <br>:book: [Laravel: Лучшие практики](https://github.com/alexeymezenin/laravel-best-practices/blob/master/russian.md) <br>|
| Полезные пакеты для Laravel |:package: [Laravel Debugbar](https://github.com/barryvdh/laravel-debugbar) <br>:package: [Laravel IDE Helper](https://github.com/barryvdh/laravel-ide-helper) <br>|


### Демонстрационный проект для начинающих: Личный блог

Для достижения начального уровня необходимо разбираться в перечисленном выше материале и уметь выполнять задания наподобие [этого](https://github.com/LaravelDaily/Laravel-Roadmap-Beginner-Challenge).

[Мой вариант решения](https://gitlab.com/dragomano/laravel-beginner-challenge)


## Материал для дополнительного изучения:

* [Сервисы, контракты и внедрение зависимостей](https://www.youtube.com/watch?v=5Kr32QFDso0)
* [Фасад, сервис-провайдер, сервис-контейнер](https://youtu.be/Ow0MrDTJ-Qo)
* [Laravel Repository Pattern – PHP Design Pattern](https://asperbrothers.com/blog/implement-repository-pattern-in-laravel/)